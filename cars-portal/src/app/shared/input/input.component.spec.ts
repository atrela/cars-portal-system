import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from '../shared.module';
import { InputComponent } from './input.component';

describe('InputComponent', () => {
  let component: InputComponent;
  let fixture: ComponentFixture<InputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit focus event', () => {
    spyOn(component.focused, 'emit');
    const inputElement: HTMLInputElement = fixture.nativeElement.querySelector('input');
    inputElement.dispatchEvent(new Event('focus'));
    expect(component.focused.emit).toHaveBeenCalled();
  });

  it('should emit blur event', () => {
    spyOn(component.blured, 'emit');
    const inputElement: HTMLInputElement = fixture.nativeElement.querySelector('input');
    inputElement.dispatchEvent(new Event('blur'));
    expect(component.blured.emit).toHaveBeenCalled();
  });
});
