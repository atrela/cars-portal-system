import { Component, EventEmitter, Input, Output } from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

@Component({
  selector: 'cp-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: InputComponent,
      multi: true,
    },
  ],
})
export class InputComponent implements ControlValueAccessor {
  constructor() {}
  @Input() label!: string;
  @Output() blured = new EventEmitter<void>();
  @Output() focused = new EventEmitter<void>();
  control = new FormControl();

  onBlur(): void {
    this.blured.emit();
  }

  onFocus(): void {
    this.focused.emit();
  }

  onInput(value: string): void {
    this.propagateChange(value);
  }

  registerOnChange(changeFunction: (value: string) => {}): void {
    this.propagateChange = changeFunction;
  }

  registerOnTouched(touchFunction: () => {}): void {
    this.propagateTouch = touchFunction;
  }

  setValue(value: string): void {
    this.control.setValue(value);
  }

  writeValue(value: string): void {
    this.control.setValue(value);
  }

  private propagateChange = (value: string) => {};

  private propagateTouch = () => {};
}
