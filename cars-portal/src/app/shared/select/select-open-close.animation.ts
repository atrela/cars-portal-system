import {
  trigger,
  state,
  style,
  transition,
  animate,
} from '@angular/animations';

export const selectOpenClose = trigger('selectOpenClose', [
  state(
    'open',
    style({
      minHeight: '40px',
      opacity: 1,
    })
  ),
  state(
    'closed',
    style({
      height: '0px',
      opacity: 0,
    })
  ),
  transition('closed => open', [animate('0.2s ease-in-out')]),
]);
