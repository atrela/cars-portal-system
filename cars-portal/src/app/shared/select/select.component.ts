import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { InputComponent } from '../input/input.component';
import { SelectOption } from '../models/select-option.model';
import { selectOpenClose } from './select-open-close.animation';

@Component({
  selector: 'cp-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  animations: [selectOpenClose],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: SelectComponent,
      multi: true,
    },
  ],
})
export class SelectComponent implements ControlValueAccessor {
  @Input() label!: string;
  @Input() options!: SelectOption[] | null;
  @Output() selected = new EventEmitter<string>();
  @ViewChild('input', { static: true }) input!: InputComponent;
  showElements = false;

  clear(): void {
    this.input.setValue('');
    this.propagateChange('');
  }

  onBlur(): void {
    this.showElements = false;
  }

  onFocus(): void {
    this.showElements = true;
  }

  registerOnChange(changeFunction: (value: string) => {}): void {
    this.propagateChange = changeFunction;
  }

  registerOnTouched(touchFunction: () => {}): void {
    this.propagateTouch = touchFunction;
  }

  selectItem(option: SelectOption): void {
    this.input.setValue(option.name);
    this.propagateChange(option.value);
  }

  writeValue(value: string): void {
    this.input.setValue(value);
  }

  private propagateChange = (value: string) => {};

  private propagateTouch = () => {};
}
