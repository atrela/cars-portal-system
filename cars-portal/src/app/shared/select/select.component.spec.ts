import { not } from '@angular/compiler/src/output/output_ast';
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';
import { InputComponent } from '../input/input.component';
import { SelectComponent } from './select.component';
const options = [
  { name: 'Test1', value: '1' },
  { name: 'Test2', value: '2' },
];
describe('SelectComponent', () => {
  let component: SelectComponent;
  let fixture: ComponentFixture<SelectComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BrowserAnimationsModule, FormsModule, ReactiveFormsModule],
      declarations: [InputComponent, SelectComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should write appropriate value into input', () => {
    selectOption();
    const inputElement: HTMLInputElement = fixture.nativeElement.querySelector('input');
    expect(inputElement.value).toBe('Test1');
  });

  it('should show list', () => {
    component.onFocus();
    fixture.detectChanges();
    const list = fixture.nativeElement.querySelector('ul');
    expect(list.style.opacity).not.toBe('0');
  });
  it('should not show list', () => {
    component.onBlur();
    fixture.detectChanges();
    const list = fixture.nativeElement.querySelector('ul');
    expect(list.style.opacity).toBe('0');
  });
  it('should clear control', () => {
    selectOption();
    component.clear();
    const inputElement: HTMLInputElement = fixture.nativeElement.querySelector('input');
    expect(inputElement.value).toBe('');
  });

  function selectOption(): void {
    component.options = options;
    component.selectItem(options[0]);
    fixture.detectChanges();
  }
});
