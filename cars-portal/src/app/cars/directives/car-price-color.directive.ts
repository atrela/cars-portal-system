import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[cpCarPriceColor]',
})
export class CarPriceColorDirective implements OnInit {
  @Input() price!: number;
  private expensive = 500000;
  private green = '#00c400';
  private medium = 300000;
  private red = '#fa8072';
  private yellow = '#ffcc00';
  constructor(private element: ElementRef) {}

  ngOnInit(): void {
    this.getAppropriateColor();
  }

  private getAppropriateColor(): void {
    let color = '';
    if (this.price >= this.expensive) {
      color = this.red;
    } else if (this.price >= this.medium) {
      color = this.yellow;
    } else {
      color = this.green;
    }
    this.element.nativeElement.style.color = color;
  }
}
