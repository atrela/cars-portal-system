import { ElementRef } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { CarPriceComponent } from 'src/app/testing/car-price.component';
import { CarPriceColorDirective } from './car-price-color.directive';

describe('CarPriceColorDirective', () => {
  let fixture: ComponentFixture<CarPriceComponent>;
  let elements: ElementRef[];
  const green = '#00c400';
  const red = '#fa8072';
  const yellow = '#ffcc00';
  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [CarPriceColorDirective, CarPriceComponent],
    }).createComponent(CarPriceComponent);

    fixture.detectChanges();
    elements = fixture.debugElement.queryAll(
      By.directive(CarPriceColorDirective)
    );
  });
  it('should have red font', () => {
    const color = elements[0].nativeElement.style.color;
    expect(color).toBe(hexToRgb(red));
  });
  it('should have yellow font', () => {
    const color = elements[1].nativeElement.style.color;
    expect(color).toBe(hexToRgb(yellow));
  });
  it('should have green font', () => {
    const color = elements[2].nativeElement.style.color;
    expect(color).toBe(hexToRgb(green));
  });

  function hexToRgb(hex: string): string | null {
    const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result
      ? `rgb(${parseInt(result[1], 16)}, ${parseInt(result[2], 16)}, ${parseInt(
          result[3],
          16
        )})`
      : null;
  }
});
