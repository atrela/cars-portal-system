import { NgModule } from '@angular/core';
import { CarsListComponent } from './components/cars-list/cars-list.component';
import { CoreModule } from '../core/core.module';
import { CarsItemComponent } from './components/cars-item/cars-item.component';
import { SharedModule } from '../shared/shared.module';
import { CarsFiltersComponent } from './components/cars-filters/cars-filters.component';
import { CarsTagPipe } from './pipes/cars-tag.pipe';
import { CarPriceColorDirective } from './directives/car-price-color.directive';
import { CarsRoutingModule } from './cars-routing.model';

@NgModule({
  declarations: [
    CarsListComponent,
    CarsItemComponent,
    CarsFiltersComponent,
    CarsTagPipe,
    CarPriceColorDirective,
  ],
  imports: [SharedModule, CoreModule, CarsRoutingModule],
  exports: [CarsListComponent],
})
export class CarsModule {}
