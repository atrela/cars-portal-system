export enum CarTag {
  Fast = 'fast',
  City = 'city',
  Luxury = 'luxury',
  Economical = 'economical',
  Safe = 'safe',
}
