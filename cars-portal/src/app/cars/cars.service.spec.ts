import { TestBed } from '@angular/core/testing';
import { CarsService } from './cars.service';

describe('CarsService', () => {
  let service: CarsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CarsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return empty array', () => {
    service.getCars({ name: 'ddd', tag: 'gg', color: 'ff' }).subscribe(result => expect(result.length).toBe(0));
  });

  it('should return cars', () => {
    service.getCars({ name: 'n', tag: '', color: '' }).subscribe(result => expect(result.length).toBeGreaterThan(0));
  });
});
