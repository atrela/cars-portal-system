import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { SelectOption } from '../shared/models';
import { cars } from './cars';
import { Car, CarsFilter } from './models';

@Injectable({
  providedIn: 'root',
})
export class CarsService {
  private filter = new BehaviorSubject<CarsFilter | null>(null);
  filter$ = this.filter.asObservable();
  constructor() {}

  getCars(filter: CarsFilter | null): Observable<Car[]> {
    return of(
      filter
        ? cars.filter(
            (car) =>
              car.tags.some((tag) => tag.includes(filter.tag)) &&
              car.name.toLowerCase().includes(filter.name.toLowerCase()) &&
              car.color.includes(filter.color)
          )
        : cars
    );
  }

  getColors(): Observable<SelectOption[]> {
    return of([
      { name: 'Beige', value: 'beige' },
      { name: 'Black', value: 'black' },
      { name: 'Blue', value: 'blue' },
      { name: 'Green', value: 'green' },
      { name: 'Red', value: 'red' },
      { name: 'Yellow', value: 'yellow' },
    ]);
  }

  getTags(): Observable<SelectOption[]> {
    return of([
      { name: 'City', value: 'city' },
      { name: 'Economical', value: 'economical' },
      { name: 'Fast', value: 'fast' },
      { name: 'Luxury', value: 'luxury' },
      { name: 'Safe', value: 'safe' },
    ]);
  }

  updateFilter(filter: CarsFilter): void {
    this.filter.next(filter);
  }
}
