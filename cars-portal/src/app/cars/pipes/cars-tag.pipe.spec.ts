import { CarTag } from '../enums/car-tag.enum';
import { CarsTagPipe } from './cars-tag.pipe';

describe('CarsTagPipe', () => {
  const pipe = new CarsTagPipe();
  it('create an instance', () => {
    expect(pipe).toBeTruthy();
  });
  it('should return city icon', () => {
    expect(pipe.transform(CarTag.City)).toBe('city-solid.svg');
  });
  it('should return piggy icon', () => {
    expect(pipe.transform(CarTag.Economical)).toBe('piggy-bank-solid.svg');
  });
  it('should return luxury icon', () => {
    expect(pipe.transform(CarTag.Luxury)).toBe('money-bill-alt-solid.svg');
  });
  it('should return burn icon', () => {
    expect(pipe.transform(CarTag.Fast)).toBe('burn-solid.svg');
  });
  it('should return shield icon', () => {
    expect(pipe.transform(CarTag.Safe)).toBe('user-shield-solid.svg');
  });
});
