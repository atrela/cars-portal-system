import { Pipe, PipeTransform } from '@angular/core';
import { CarTag } from '../enums/car-tag.enum';

@Pipe({
  name: 'carsTag',
})
export class CarsTagPipe implements PipeTransform {
  transform(tag: CarTag): string {
    switch (tag) {
      case CarTag.City:
        return 'city-solid.svg';
      case CarTag.Economical:
        return 'piggy-bank-solid.svg';
      case CarTag.Luxury:
        return 'money-bill-alt-solid.svg';
      case CarTag.Fast:
        return 'burn-solid.svg';
      case CarTag.Safe:
        return 'user-shield-solid.svg';
    }
  }
}
