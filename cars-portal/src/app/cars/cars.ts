import { CarTag } from './enums/car-tag.enum';

export const cars = [
  {
    id: '1',
    name: 'Caddilac CT4',
    img: 'car_1.png',
    color: 'green',
    price: 100000,
    year: 1920,
    country: 'Russia',
    tags: [CarTag.Economical, CarTag.Safe]

  },
  {
    id: '2',
    name: 'Cad Eldorado',
    img: 'car_2.png',
    color: 'blue',
    price: 123000,
    year: 1950,
    country: 'Austria',
    tags: [CarTag.Economical, CarTag.Safe]
  },
  {
    id: '3',
    name: 'Mercedes Classic',
    img: 'car_3.png',
    color: 'beige',
    price: 230000,
    year: 1921,
    country: 'Hungary',
    tags: [CarTag.Fast]
  },
  {
    id: '4',
    name: 'Chevy Corvette',
    img: 'car_4.png',
    color: 'blue',
    price: 3200000,
    year: 1930,
    country: 'Austria',
    tags: [CarTag.Economical, CarTag.Fast]
  },
  {
    id: '5',
    name: 'Shelby GT350',
    img: 'car_5.png',
    color: 'black',
    price: 400000,
    year: 1850,
    country: 'Romania',
    tags: [CarTag.Fast, CarTag.City]
  },
  {
    id: '6',
    name: 'Cadillac DeVille',
    img: 'car_6.png',
    color: 'black',
    price: 232900,
    year: 1911,
    country: 'USA',
    tags: [CarTag.Economical, CarTag.Fast]
  },
  {
    id: '7',
    name: 'Volvo P1800',
    img: 'car_7.png',
    color: 'blue',
    price: 122345,
    year: 1921,
    country: 'Russia',
    tags: [CarTag.City, CarTag.Fast]
  },
  {
    id: '8',
    name: 'Cad SuperPower',
    img: 'car_8.png',
    color: 'black',
    price: 500000,
    year: 1878,
    country: 'Russia',
    tags: [CarTag.Luxury, CarTag.Fast]
  },
  {
    id: '9',
    name: 'Dodge Viper GTS',
    img: 'car_9.png',
    color: 'red',
    price: 2234555,
    year: 1967,
    country: 'Germany',
    tags: [CarTag.Economical]
  },
  {
    id: '10',
    name: 'DeTomaso',
    img: 'car_10.png',
    color: 'red',
    price: 355000,
    year: 1923,
    country: 'Poland',
    tags: [CarTag.Safe]
  },
  {
    id: '11',
    name: 'Dawn Drophead',
    img: 'car_11.png',
    color: 'red',
    price: 280000,
    year: 1933,
    country: 'Russia',
    tags: [CarTag.Economical, CarTag.Fast]
  },
  {
    id: '12',
    name: 'McLaren F1',
    img: 'car_12.png',
    color: 'red',
    price: 234000,
    year: 1867,
    country: 'Canada',
    tags: [CarTag.Economical, CarTag.Fast]
  },
  {
    id: '13',
    name: 'Cizeta V16T',
    img: 'car_13.png',
    color: 'beige',
    price: 555000,
    year: 1888,
    country: 'Russia',
    tags: [CarTag.Economical, CarTag.Fast]
  },
  {
    id: '14',
    name: 'Datsun 240Z',
    img: 'car_14.png',
    color: 'beige',
    price: 232000,
    year: 1877,
    country: 'Germany',
    tags: [CarTag.Luxury, CarTag.Safe]
  },
  {
    id: '15',
    name: 'Chevrolet',
    img: 'car_15.png',
    color: 'red',
    price: 655000,
    year: 1945,
    country: 'USA',
    tags: [CarTag.Safe]
  },
  {
    id: '16',
    name: 'VW Beetle',
    img: 'car_16.png',
    color: 'yellow',
    price: 330111,
    year: 1900,
    country: 'UK',
    tags: [CarTag.Luxury, CarTag.Safe]
  },
  {
    id: '17',
    name: 'VW Beetle',
    img: 'car_17.png',
    color: 'blue',
    price: 344000,
    year: 1922,
    country: 'Romania',
    tags: [CarTag.Economical, CarTag.Safe]
  },
  {
    id: '18',
    name: 'Vintage Jeep',
    img: 'car_18.png',
    color: 'blue',
    price: 455000,
    year: 1956,
    country: 'Poland',
    tags: [CarTag.City, CarTag.Safe]
  },
  {
    id: '19',
    name: 'Ford Model T',
    img: 'car_19.png',
    color: 'beige',
    price: 233000,
    year: 1966,
    country: 'Slovakia',
    tags: [CarTag.Luxury]
  },
  {
    id: '20',
    name: 'Fiat 124 Spider',
    img: 'car_20.png',
    color: 'red',
    price: 355999,
    year: 1912,
    country: 'Russia',
    tags: [CarTag.Economical]
  },
];
