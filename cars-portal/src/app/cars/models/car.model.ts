import { CarTag } from '../enums/car-tag.enum';

export interface Car {
  id: string;
  name: string;
  year: number;
  color: string;
  country: string;
  tags: CarTag[];
  img: string;
  price: number;
}
