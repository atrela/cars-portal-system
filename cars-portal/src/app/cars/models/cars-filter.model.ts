export interface CarsFilter {
    name: string;
    tag: string;
    color: string;
}
