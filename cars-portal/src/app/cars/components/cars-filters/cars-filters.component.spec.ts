import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/shared/shared.module';
import { MockCarsService } from 'src/app/testing/mock-cars-service';
import { CarsService } from '../../cars.service';
import { CarsItemComponent } from '../cars-item/cars-item.component';
import { CarsFiltersComponent } from './cars-filters.component';

describe('CarsFiltersComponent', () => {
  let component: CarsFiltersComponent;
  let fixture: ComponentFixture<CarsFiltersComponent>;
  let service: CarsService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CarsFiltersComponent, CarsItemComponent],
      imports: [SharedModule, NoopAnimationsModule],
      providers: [{ provide: CarsService, useClass: MockCarsService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsFiltersComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(CarsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should notify about filter changes', () => {
    const userServiceSpy = spyOn(service, 'updateFilter').and.callThrough();
    triggerFilters();
    expect(userServiceSpy).toHaveBeenCalledTimes(1);
  });

  it('should notify about appropriate changes', () => {
    const userServiceSpy = spyOn(service, 'updateFilter').and.callThrough();
    triggerFilters();
    expect(userServiceSpy).toHaveBeenCalledWith({
      color: 'green',
      tag: '',
      name: '',
    });
  });

  it('should be possible to reset filters', () => {
    component.reset();
    expect(component.color.value).toBe('');
    expect(component.tag.value).toBe('');
    expect(component.name.value).toBe('');
  });

  function triggerFilters(): void {
    component.color.setValue('green');
  }
});
