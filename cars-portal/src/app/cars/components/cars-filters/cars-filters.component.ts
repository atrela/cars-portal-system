import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, Observable, Subscription } from 'rxjs';
import { SelectOption } from 'src/app/shared/models';
import { CarsService } from '../../cars.service';
import { startWith } from 'rxjs/operators';

@Component({
  selector: 'cp-cars-filters',
  templateUrl: './cars-filters.component.html',
  styleUrls: ['./cars-filters.component.scss'],
})
export class CarsFiltersComponent implements OnInit, OnDestroy {
  color = new FormControl();
  colors$!: Observable<SelectOption[]>;
  name = new FormControl();
  tag = new FormControl();
  tags$!: Observable<SelectOption[]>;
  private subscription!: Subscription;
  constructor(private carsService: CarsService) {}

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngOnInit(): void {
    this.initializeData();
  }

  reset(): void {
    this.name.setValue('');
    this.color.setValue('');
    this.tag.setValue('');
  }

  private initializeData(): void {
    this.colors$ = this.carsService.getColors();
    this.tags$ = this.carsService.getTags();
    this.subscription = combineLatest([
      this.name.valueChanges.pipe(startWith('')),
      this.tag.valueChanges.pipe(startWith('')),
      this.color.valueChanges.pipe(startWith('')),
    ]).subscribe(([name, tag, color]) => {
      this.carsService.updateFilter({ name, tag, color });
    });
  }
}
