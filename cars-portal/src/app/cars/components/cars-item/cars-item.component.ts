import { Component, Input } from '@angular/core';
import { Car } from '../../models';

@Component({
  selector: 'cp-cars-item',
  templateUrl: './cars-item.component.html',
  styleUrls: ['./cars-item.component.scss'],
})
export class CarsItemComponent {
  @Input() car!: Car;
}
