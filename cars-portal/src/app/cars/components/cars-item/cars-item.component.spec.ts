import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CarPriceColorDirective } from '../../directives/car-price-color.directive';
import { CarTag } from '../../enums/car-tag.enum';
import { CarsTagPipe } from '../../pipes/cars-tag.pipe';
import { CarsItemComponent } from './cars-item.component';
const car = {
  id: '2',
  name: 'Cad Eldorado',
  img: 'car_2.png',
  color: 'blue',
  price: 123000,
  year: 1950,
  country: 'Austria',
  tags: [CarTag.Economical, CarTag.Safe],
};
describe('CarsItemComponent', () => {
  let component: CarsItemComponent;
  let fixture: ComponentFixture<CarsItemComponent>;
  let compiled: HTMLElement;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule],
      declarations: [CarsItemComponent, CarsTagPipe, CarPriceColorDirective],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsItemComponent);
    component = fixture.componentInstance;
    component.car = car;
    fixture.detectChanges();
    compiled = fixture.debugElement.nativeElement;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display car name', () => {
    const name = compiled.querySelector('.card__name');
    expect(name?.innerHTML).toBe(car.name);
  });

  it('should display car production year', () => {
    const year = compiled.querySelectorAll('.card__info-title + span')[0];
    expect(year?.innerHTML).toBe(car.year.toString());
  });

  it('should display car price', () => {
    const price = compiled.querySelectorAll('.card__info-title + span')[1];
    expect(price?.innerHTML).toBe('$123,000.00');
  });

  it('should display car country', () => {
    const country = compiled.querySelectorAll('.card__info-title + span')[2];
    expect(country?.innerHTML).toBe(car.country);
  });
  it('should display correct number of tags', () => {
    const tags = compiled.querySelectorAll('.card__tag');
    expect(tags.length).toBe(car.tags.length);
  });
});
