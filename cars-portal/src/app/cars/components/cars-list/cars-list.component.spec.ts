import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from 'src/app/core/header/header.component';
import { SharedModule } from 'src/app/shared/shared.module';
import { MockCarsService } from 'src/app/testing/mock-cars-service';
import { CarsService } from '../../cars.service';
import { CarPriceColorDirective } from '../../directives/car-price-color.directive';
import { CarsTagPipe } from '../../pipes/cars-tag.pipe';
import { CarsFiltersComponent } from '../cars-filters/cars-filters.component';
import { CarsItemComponent } from '../cars-item/cars-item.component';
import { CarsListComponent } from './cars-list.component';

describe('CarsListComponent', () => {
  let component: CarsListComponent;
  let fixture: ComponentFixture<CarsListComponent>;
  let service: CarsService;
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [SharedModule, NoopAnimationsModule],
      declarations: [
        CarsListComponent,
        CarsItemComponent,
        CarsTagPipe,
        CarsFiltersComponent,
        CarPriceColorDirective,
        HeaderComponent
      ],
      providers: [{ provide: CarsService, useClass: MockCarsService }],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarsListComponent);
    component = fixture.componentInstance;
    service = TestBed.inject(CarsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch cars', () => {
    service.updateFilter({ color: 'green', name: '', tag: '' });
    fixture.detectChanges();
    const cars = getCars();
    expect(cars.length).toBeGreaterThan(0);
  });

  it('should not show cars', () => {
    service.updateFilter({ color: 'green', name: '54545', tag: '454545' });
    fixture.detectChanges();
    const cars = getCars();
    expect(cars.length).toBe(0);
  });

  it('should show no items message', () => {
    service.updateFilter({ color: 'green', name: '54545', tag: '454545' });
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    const title = compiled.querySelector('.cars-block__title');
    expect(title).not.toBe(null);
  });

  function getCars(): HTMLElement[] {
    const compiled = fixture.debugElement.nativeElement;
    return compiled.querySelectorAll('cp-cars-item');
  }
});
