import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { CarsService } from '../../cars.service';
import { Car } from '../../models';

@Component({
  selector: 'cp-cars-list',
  templateUrl: './cars-list.component.html',
  styleUrls: ['./cars-list.component.scss'],
})
export class CarsListComponent implements OnInit {
  cars$!: Observable<Car[]>;
  constructor(private carsService: CarsService) {}

  ngOnInit(): void {
    this.cars$ = this.carsService.filter$.pipe(
      mergeMap((filterValue) => {
        return this.carsService.getCars(filterValue);
      })
    );
  }
}


