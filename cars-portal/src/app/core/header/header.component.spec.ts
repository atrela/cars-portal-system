import { ComponentFixture, inject, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DummyComponent } from 'src/app/testing/dummy.component';
import { HeaderComponent } from './header.component';
import { Location } from '@angular/common';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: DummyComponent },
        ]),
      ],
      declarations: [HeaderComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    'should go to main page',
    waitForAsync(
      inject([Location], (location: Location) => {
        const compiled = fixture.debugElement.nativeElement;
        compiled.querySelector('a').click();
        fixture.whenStable().then(() => {
          expect(location.path()).toEqual('/');
        });
      })
    )
  );
});
