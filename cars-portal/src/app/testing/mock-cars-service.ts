import { BehaviorSubject, Observable, of } from 'rxjs';
import { cars } from '../cars/cars';
import { Car, CarsFilter } from '../cars/models';
import { SelectOption } from '../shared/models';

export class MockCarsService {
  private filter = new BehaviorSubject<CarsFilter | null>(null);
  filter$ = this.filter.asObservable();
  constructor() {}

  getCars(filter: CarsFilter | null): Observable<Car[]> {
    return of(
      filter
        ? cars.filter(
            (car) =>
              car.tags.some((tag) => tag.includes(filter.tag)) &&
              car.name.toLowerCase().includes(filter.name.toLowerCase()) &&
              car.color.includes(filter.color)
          )
        : cars
    );
  }

  getColors(): Observable<SelectOption[]> {
    return of([
      { name: 'Beige', value: 'beige' },
      { name: 'Black', value: 'black' },
      { name: 'Blue', value: 'blue' },
      { name: 'Green', value: 'green' },
      { name: 'Red', value: 'red' },
      { name: 'Yellow', value: 'yellow' },
    ]);
  }

  getTags(): Observable<SelectOption[]> {
    return of([
      { name: 'City', value: 'city' },
      { name: 'Economical', value: 'economical' },
      { name: 'Expensive', value: 'expensive' },
      { name: 'Fast', value: 'fast' },
      { name: 'Safe', value: 'safe' },
    ]);
  }

  updateFilter(filter: CarsFilter): void {
    this.filter.next(filter);
  }
}
