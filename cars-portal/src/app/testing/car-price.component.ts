import { Component } from '@angular/core';

@Component({
    template: `
    <h2 cpCarPriceColor [price]="500000">Something red</h2>
    <h2 cpCarPriceColor [price]="300000">Something yellow</h2>
    <h2 cpCarPriceColor [price]="22323">Something green</h2>`
  })
  export class CarPriceComponent { }
