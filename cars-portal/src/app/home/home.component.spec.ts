import {
  ComponentFixture,
  inject,
  TestBed,
  waitForAsync,
} from '@angular/core/testing';
import { Location } from '@angular/common';
import { HomeComponent } from './home.component';
import { RouterLinkDirective } from '../testing/router-link.directive';
import { RouterTestingModule } from '@angular/router/testing';
import { DummyComponent } from '../testing/dummy.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: 'car', component: DummyComponent },
        ]),
      ],
      declarations: [HomeComponent, RouterLinkDirective],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(
    'should go to cars',
    waitForAsync(
      inject([Location], (location: Location) => {
        const compiled = fixture.debugElement.nativeElement;
        compiled.querySelector('button').click();
        fixture.whenStable().then(() => {
          expect(location.path()).toEqual('/car');
        });
      })
    )
  );
});
