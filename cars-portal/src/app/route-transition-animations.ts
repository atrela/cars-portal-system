import {
  trigger,
  transition,
  style,
  query,
  animateChild,
  group,
  animate,
} from '@angular/animations';

export const routeTransitionAnimations = trigger('triggerName', [
  transition('Home => Cars', [
    style({ position: 'relative' }),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        top: 0,
        right: 0,
        width: '100%',
      }),
    ]),
    query(':enter', [style({ right: '-100%', opacity: 0 })], { optional: true }),
    query(':leave', animateChild()),
    group([
      query(':leave', [
        animate('700ms ease-out', style({ right: '100%', opacity: 0 })),
      ]),
      query(':enter', [
        animate('700ms ease-out', style({ right: '0%', opacity: 1 })),
      ], { optional: true }),
    ]),
    query(':enter', animateChild(), { optional: true }),
  ]),
  transition('Cars => Home', [
    style({ position: 'relative' }),
    query(':enter, :leave', [
      style({
        position: 'absolute',
        top: 0,
        left: 0,
        width: '100%',
      }),
    ]),
    query(':enter', [style({ left: '-100%', opacity: 0 })]),
    query(':leave', animateChild()),
    group([
      query(':leave', [
        animate('700ms ease-out', style({ left: '100%', opacity: 0 })),
      ]),
      query(':enter', [
        animate('700ms ease-out', style({ left: '0%', opacity: 1 })),
      ]),
    ]),
    query(':enter', animateChild()),
  ]),
]);
