import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: 'car',
    loadChildren: () => import('./cars/cars.module').then((m) => m.CarsModule),
    data: { animationState: 'Cars' },
  },
  {
    path: '',
    component: HomeComponent,
    data: { animationState: 'Home' },
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled',
    }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
